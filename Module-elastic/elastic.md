# Домашнее задание к занятию "6.5. Elasticsearch"

## Задача 1

В этом задании вы потренируетесь в:
- установке elasticsearch
- первоначальном конфигурировании elastcisearch
- запуске elasticsearch в docker

Используя докер образ [centos:7](https://hub.docker.com/_/centos) как базовый и 
[документацию по установке и запуску Elastcisearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/targz.html):

- составьте Dockerfile-манифест для elasticsearch
- соберите docker-образ и сделайте `push` в ваш docker.io репозиторий
- запустите контейнер из получившегося образа и выполните запрос пути `/` c хост-машины

Требования к `elasticsearch.yml`:
- данные `path` должны сохраняться в `/var/lib`
- имя ноды должно быть `netology_test`

В ответе приведите:
- текст Dockerfile манифеста
- ссылку на образ в репозитории dockerhub
- ответ `elasticsearch` на запрос пути `/` в json виде

Подсказки:
- возможно вам понадобится установка пакета perl-Digest-SHA для корректной работы пакета shasum
- при сетевых проблемах внимательно изучите кластерные и сетевые настройки в elasticsearch.yml
- при некоторых проблемах вам поможет docker директива ulimit
- elasticsearch в логах обычно описывает проблему и пути ее решения

Далее мы будем работать с данным экземпляром elasticsearch.

Ответ:

1)  Dockerfile
#6.5. Elasticsearch
FROM centos:7
LABEL ElasticSearch Lab 6.5 \
    (c) Korneev Roman

ENV PATH=/usr/lib:$PATH

RUN rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch

RUN echo "[elasticsearch]" >>/etc/yum.repos.d/elasticsearch.repo &&\
    echo "name=Elasticsearch repository for 7.x packages" >>/etc/yum.repos.d/elasticsearch.repo &&\
    echo "baseurl=https://artifacts.elastic.co/packages/7.x/yum">>/etc/yum.repos.d/elasticsearch.repo &&\
    echo "gpgcheck=1">>/etc/yum.repos.d/elasticsearch.repo &&\
    echo "gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch">>/etc/yum.repos.d/elasticsearch.repo &&\
    echo "enabled=0">>/etc/yum.repos.d/elasticsearch.repo &&\
    echo "autorefresh=1">>/etc/yum.repos.d/elasticsearch.repo &&\
    echo "type=rpm-md">>/etc/yum.repos.d/elasticsearch.repo 

RUN yum install -y --enablerepo=elasticsearch elasticsearch 

    
ADD elasticsearch.yml /etc/elasticsearch/

RUN mkdir /usr/share/elasticsearch/snapshots &&\
    chown elasticsearch:elasticsearch /usr/share/elasticsearch/snapshots

RUN mkdir /var/lib/logs \
    && chown elasticsearch:elasticsearch /var/lib/logs \
    && mkdir /var/lib/data \
    && chown elasticsearch:elasticsearch /var/lib/data
    
USER elasticsearch

CMD ["/usr/sbin/init"]

CMD ["/usr/share/elasticsearch/bin/elasticsearch"]

2) ссылка на образ

https://hub.docker.com/repository/docker/rmx7799/elastic

3) ответ на запрос GET /

{

  "name" : "723b62151482",

  "cluster_name" : "netology_test",

  "cluster_uuid" : "BdmKTClrU61boEmP952vNu",

  "version" : {

    "number" : "7.11.1",

    "build_flavor" : "default",

    "build_type" : "rpm",

    "build_hash" : "2ff76273be3c3203311f504e636a66478e97ce63",

    "build_date" : "2022-05-26T22:12:42.402131Z",

    "build_snapshot" : false,

    "lucene_version" : "8.7.0",

    "minimum_wire_compatibility_version" : "6.8.0",

    "minimum_index_compatibility_version" : "6.0.0-beta1"

  },
  "tagline" : "You Know, for Search"
}

## Задача 2

В этом задании вы научитесь:
- создавать и удалять индексы
- изучать состояние кластера
- обосновывать причину деградации доступности данных

Ознакомтесь с [документацией](https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-create-index.html) 
и добавьте в `elasticsearch` 3 индекса, в соответствии со таблицей:

| Имя | Количество реплик | Количество шард |
|-----|-------------------|-----------------|
| ind-1| 0 | 1 |
| ind-2 | 1 | 2 |
| ind-3 | 2 | 4 |

Получите список индексов и их статусов, используя API и **приведите в ответе** на задание.

Получите состояние кластера `elasticsearch`, используя API.

Как вы думаете, почему часть индексов и кластер находится в состоянии yellow?

Удалите все индексы.

**Важно**

При проектировании кластера elasticsearch нужно корректно рассчитывать количество реплик и шард,
иначе возможна потеря данных индексов, вплоть до полной, при деградации системы.


Ответ:

Создавать индексы:

curl -X PUT localhost:9200/ind-1 -H 'Content-Type: application/json' -d'{ "settings": { "number_of_shards": 1,  "number_of_replicas": 0 }}'

curl -X PUT localhost:9200/ind-2 -H 'Content-Type: application/json' -d'{ "settings": { "number_of_shards": 2,  "number_of_replicas": 1 }}'

curl -X PUT localhost:9200/ind-3 -H 'Content-Type: application/json' -d'{ "settings": { "number_of_shards": 4,  "number_of_replicas": 2 }}' 

Получить список индексов:


$ curl -X GET 'http://localhost:9200/_cat/indices?v' 

health status index uuid                   pri rep docs.count docs.deleted store.size pri.store.
size

green  open   ind-1 Yb75xoTDSojwqV81Mwu23a   1   0          0            0       208b           208b

yellow open   ind-3 Nhiuvbwe7bXZI0X98hnq7b   4   2          0            0       832b           832b

yellow open   ind-2 B5bYBGE7DnkRQbVNyn-5Le   2   1          0            0       416b           416b


Статус индексов:

$ curl -X GET 'http://localhost:9200/_cluster/health/ind-1?pretty' 

{
  "cluster_name" : "netology_test",

  "status" : "green",

  "timed_out" : false,

  "number_of_nodes" : 1,

  "number_of_data_nodes" : 1,

  "active_primary_shards" : 1,

  "active_shards" : 1,

  "relocating_shards" : 0,

  "initializing_shards" : 0,

  "unassigned_shards" : 0,

  "delayed_unassigned_shards" : 0,

  "number_of_pending_tasks" : 0,

  "number_of_in_flight_fetch" : 0,

  "task_max_waiting_in_queue_millis" : 0,

  "active_shards_percent_as_number" : 100.0

}

$ curl -X GET 'http://localhost:9200/_cluster/health/ind-2?pretty' 

{
  "cluster_name" : "netology_test",

  "status" : "yellow",

  "timed_out" : false,

  "number_of_nodes" : 1,

  "number_of_data_nodes" : 1,

  "active_primary_shards" : 2,

  "active_shards" : 2,

  "relocating_shards" : 0,

  "initializing_shards" : 0,

  "unassigned_shards" : 2,

  "delayed_unassigned_shards" : 0,

  "number_of_pending_tasks" : 0,

  "number_of_in_flight_fetch" : 0,

  "task_max_waiting_in_queue_millis" : 0,

  "active_shards_percent_as_number" : 41.17647058823529

}

$ curl -X GET 'http://localhost:9200/_cluster/health/ind-3?pretty' 

{

  "cluster_name" : "netology_test",

  "status" : "yellow",

  "timed_out" : false,

  "number_of_nodes" : 1,

  "number_of_data_nodes" : 1,

  "active_primary_shards" : 4,

  "active_shards" : 4,

  "relocating_shards" : 0,

  "initializing_shards" : 0,

  "unassigned_shards" : 8,

  "delayed_unassigned_shards" : 0,

  "number_of_pending_tasks" : 0,

  "number_of_in_flight_fetch" : 0,

  "task_max_waiting_in_queue_millis" : 0,

  "active_shards_percent_as_number" : 41.17647058823529

}

Статус кластера:

$ curl -XGET localhost:9200/_cluster/health/?pretty=true

{
  "cluster_name" : "netology_test",

  "status" : "yellow",

  "timed_out" : false,

  "number_of_nodes" : 1,

  "number_of_data_nodes" : 1,

  "active_primary_shards" : 7,

  "active_shards" : 7,

  "relocating_shards" : 0,

  "initializing_shards" : 0,

  "unassigned_shards" : 10,

  "delayed_unassigned_shards" : 0,

  "number_of_pending_tasks" : 0,

  "number_of_in_flight_fetch" : 0,

  "task_max_waiting_in_queue_millis" : 0,

  "active_shards_percent_as_number" : 41.17647058823529

}
Удаление индексов:

$ curl -X DELETE 'http://localhost:9200/ind-1?pretty' 

{

  "acknowledged" : true

}

$ curl -X DELETE 'http://localhost:9200/ind-2?pretty' 

{

  "acknowledged" : true

}

$ curl -X DELETE 'http://localhost:9200/ind-3?pretty' 

{

  "acknowledged" : true

}

$ curl -X GET 'http://localhost:9200/_cat/indices?v'


health status index uuid pri rep docs.count docs.deleted store.size pri.store.size


Индексы в статусе Yellow потому что для индексов указано число реплик.

Но нет других серверов, соответсвено реплицировать нет возможности.


## Задача 3

В данном задании вы научитесь:
- создавать бэкапы данных
- восстанавливать индексы из бэкапов

Создайте директорию `{путь до корневой директории с elasticsearch в образе}/snapshots`.

Используя API [зарегистрируйте](https://www.elastic.co/guide/en/elasticsearch/reference/current/snapshots-register-repository.html#snapshots-register-repository) 
данную директорию как `snapshot repository` c именем `netology_backup`.

**Приведите в ответе** запрос API и результат вызова API для создания репозитория.

Создайте индекс `test` с 0 реплик и 1 шардом и **приведите в ответе** список индексов.

[Создайте `snapshot`](https://www.elastic.co/guide/en/elasticsearch/reference/current/snapshots-take-snapshot.html) 
состояния кластера `elasticsearch`.

**Приведите в ответе** список файлов в директории со `snapshot`ами.

Удалите индекс `test` и создайте индекс `test-2`. **Приведите в ответе** список индексов.

[Восстановите](https://www.elastic.co/guide/en/elasticsearch/reference/current/snapshots-restore-snapshot.html) состояние
кластера `elasticsearch` из `snapshot`, созданного ранее. 

**Приведите в ответе** запрос к API восстановления и итоговый список индексов.

Подсказки:
- возможно вам понадобится доработать `elasticsearch.yml` в части директивы `path.repo` и перезапустить `elasticsearch`

Ответ:

$ curl -XPOST localhost:9200/_snapshot/netology_backup?pretty -H 'Content-Type: application/

json' -d'{"type": "fs", "settings": { "location":"/usr/share/elasticsearch/snapshots" }}'

{

  "acknowledged" : true

}

результат: http://localhost:9200/_snapshot/netology_backup?pretty

{

  "netology_backup" : {

    "type" : "fs",

    "settings" : {

      "location" : "/usr/share/elasticsearch/snapshots"

    }

  }

}


curl -X PUT localhost:9200/test -H 'Content-Type: application/json' -d'{ "settings": {
   "number_of_shards": 1,  "number_of_replicas": 0 }}'

{"acknowledged":true,"shards_acknowledged":true,"index":"test"}1

Результат: http://localhost:9200/test?pretty

{

  "test" : {

    "aliases" : { },

    "mappings" : { },

    "settings" : {

      "index" : {

        "routing" : {

          "allocation" : {

            "include" : {

              "_tier_preference" : "data_content"

            }

          }

        },

        "number_of_shards" : "1",

        "provided_name" : "test",

        "creation_date" : "1615033116650",

        "number_of_replicas" : "0",

        "uuid" : "P1duHjRTQBacJyWPGeNwUQ",

        "version" : {

          "created" : "7110199"

        }

      }

    }

  }

}


$ curl -X PUT localhost:9200/_snapshot/netology_backup/elasticsearch?wait_for_completion=true
{"snapshot":{"snapshot":"elasticsearch","uuid":"wixOT9zMS_WYXlGfNw7nsQ","version_id":7110199,"version":"7.11.1","indices":["test"],"data_streams":[],"include_global_state":true,"state":"SUCCESS","start_time":"2022-05-26T22:12:23:31.388Z","start_time_in_millis":16150334176912,"end_time":"2022-05-26T22:12:23:31.988Z","end_time_in_millis":1615033411988,"duration_in_millis":600,"failures":[],"shards":{"total":1,"failed":0,"successful":1}}}


Результат:

vagrant@vagrant$ pwd

/usr/share/elasticsearch/snapshots

vagrant@vagrant$ ls -la

total 85

drwxr-xr-x 3 elasticsearch elasticsearch  4096 May  26 22:54 .

drwxr-xr-x 9 root          root           4096 May  26 22:13 ..

-rw-r--r-- 1 elasticsearch elasticsearch   692 May  26 22:54 index-1

-rw-r--r-- 1 elasticsearch elasticsearch     8 May  26 22:54 index.latest

drwxr-xr-x 3 elasticsearch elasticsearch  4096 May  26 22:23 indices

-rw-r--r-- 1 elasticsearch elasticsearch 30931 May  26 22:54 meta-N127nByqweq8nmN1Uz2.dat

-rw-r--r-- 1 elasticsearch elasticsearch 30931 May  26 22:23 meta-bmuHY7823mBx7wn1bn1.dat

-rw-r--r-- 1 elasticsearch elasticsearch   267 May  26 22:54 snap-msk32Bnu71b3TvR911a.dat

-rw-r--r-- 1 elasticsearch elasticsearch   269 May  26 22:23 snap-n2uNhy198mloPmV192d.dat

vagrant@vagrant:~/docker$ 


Удаление и создание нового индекса:

health status index uuid                   pri rep docs.count docs.deleted store.size pri.store.
size


green  open   test  Y7823mBx7wsam129j12sadF   1   0          0            0       208b           208b



vagrant@vagrant:~/docker$ curl -X DELETE 'http://localhost:9200/test?pretty'


{

    "acknowledged" : true

}

vagrant@vagrant:~/docker$ curl -X PUT localhost:9200/test-2?pretty -H 'Content-Type: application/json' -d'{ "settings": { "number_of_shards": 1,  "number_of_replicas": 0 }}'

{

  "acknowledged" : true,
  
  "shards_acknowledged" : true,

  "index" : "test-2"

}


health status index  uuid                   pri rep docs.count docs.deleted store.size pri.store.size

green  open   test-2 Nu8i2n&31b0nuNM19mAf   1   0          0            0       208b           208b


vagrant@vagrant:~/docker$ curl -X POST localhost:9200/_snapshot/netology_backup/elasticsearch/_restore?pretty -H 'Content-Type: application/json' -d'{"include_global_state":true}'

{


  "accepted" : true

}


vagrant@vagrant:~/docker$ curl -X GET http://localhost:9200/_cat/indices?v

health status index  uuid                   pri rep docs.count docs.deleted store.size pri.store.size

green  open   test-2 1b0nuNM19mAfB71bn9012aF   1   0          0            0       208b           208b
green  open   test   M23nm1&nq31b0nuNM19mAve   1   0          0            0       208b           208b

---

### Как cдавать задание

Выполненное домашнее задание пришлите ссылкой на .md-файл в вашем репозитории.

---