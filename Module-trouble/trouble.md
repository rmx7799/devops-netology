# Домашнее задание к занятию "6.6. Troubleshooting"

## Задача 1

Перед выполнением задания ознакомьтесь с документацией по [администрированию MongoDB](https://docs.mongodb.com/manual/administration/).

Пользователь (разработчик) написал в канал поддержки, что у него уже 3 минуты происходит CRUD операция в MongoDB и её 
нужно прервать. 

Вы как инженер поддержки решили произвести данную операцию:
- напишите список операций, которые вы будете производить для остановки запроса пользователя

Ответ:

Для определения текущей операци:  

   db.currentOp()

Для завершения операции по opid

   db.killOp()


- предложите вариант решения проблемы с долгими (зависающими) запросами в MongoDB

Ответ:

1. С помощью Database Profiler выявить медленные операции. Используя executionStats проанализировать. 
Возможна оптимизация: добавиние или удаление индексов, настройка шардинга.

2. для установки предела исполнения по времени операций возможно использование метода maxTimeMS() ; 


## Задача 2

Перед выполнением задания познакомьтесь с документацией по [Redis latency troobleshooting](https://redis.io/topics/latency).

Вы запустили инстанс Redis для использования совместно с сервисом, который использует механизм TTL. 
Причем отношение количества записанных key-value значений к количеству истёкших значений есть величина постоянная и
увеличивается пропорционально количеству реплик сервиса. 

При масштабировании сервиса до N реплик вы увидели, что:
- сначала рост отношения записанных значений к истекшим
- Redis блокирует операции записи

Как вы думаете, в чем может быть проблема?


Ответ:

Учитывая официальную документацию напрашивается вывод, что память забивается истекшими ключами.
В качестве решения проблемы можно поменять алгоритм отчистки просроченных ключей, т.е. установить адаптивный режим исключения просроченных ключей (ACTIVE_EXPIRE_CYCLE_LOOKUPS_PER_LOOP). При использовании алгоритма отчистки просроченных ключей выполняется механизм - при достижении свыше 25% просроченных ключей, старые просроченные ключи будут вытесняться новыми.


 
## Задача 3

Вы подняли базу данных MySQL для использования в гис-системе. При росте количества записей, в таблицах базы,
пользователи начали жаловаться на ошибки вида:
```python
InterfaceError: (InterfaceError) 2013: Lost connection to MySQL server during query u'SELECT..... '
```

Как вы думаете, почему это начало происходить и как локализовать проблему?

Какие пути решения данной проблемы вы можете предложить?

Ответ:

1. Недостаточное значение параметра connect_timeout и клиент не успевает установить соединение.

2. Черезчур объемные запросы, как вариант решения - можно увеличить параметр net_read_timeout;

3. Размер сообщения или запроса превышает размер буфера, нужно поправить параметры max_allowed_packet на сервере или max_allowed_packet на строне клиента.



## Задача 4


Вы решили перевести гис-систему из задачи 3 на PostgreSQL, так как прочитали в документации, что эта СУБД работает с 
большим объемом данных лучше, чем MySQL.

После запуска пользователи начали жаловаться, что СУБД время от времени становится недоступной. В dmesg вы видите, что:

`postmaster invoked oom-killer`

Как вы думаете, что происходит?

Как бы вы решили данную проблему?

---

Ответ:

Сообщение указывает на то, что операционной системе, приложениям и СУБД PostgreSQL не хватает оперативной памяти. В операционной системе есть два варианта решения данной проблемы : или остановка ОС, или завершение процесса. Именно для этого запускается процесс Out-Of-Memory Killer.

В качестве решения данной проблемы возможны варианты:

1. Увеличение оперативной памяти;

2. Завершение менее критичных (важных) процессов, чтобы освободить оперативную память;

3. Посмотреть и при необходимости перенастроить параметры отвечающие за работу PostgreSQL с памятью:

   - maintenance_work_mem.

   - work_mem;

   - shared_buffers;

   - effective_cache_size$



### Как cдавать задание

Выполненное домашнее задание пришлите ссылкой на .md-файл в вашем репозитории.

---